//
//  ViewController.swift
//  ez
//
//  Created by Bayu on 12/19/16.
//  Copyright © 2016 Dycode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var labelLoad: UILabel!
    @IBOutlet weak var buttonChange: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelLoad.text = "Something else"
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func labelChange(sender: UIButton, forEvent event: UIEvent) {
        request().changeLabel(labelLoad, sender: nil, text: "Hello there")
    }

}

